import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class GuruTest {

    @Test
    public void firstTest() {
        System.setProperty("webdriver.chrome.driver", "/Users/ruslanrakhmonov/IdeaProjects/comseleniumpractice/src/main/resources/chromedriver");
        WebDriver myDriver = new ChromeDriver();
        myDriver.get("https://ithillel.ua/ua");
        assertTrue(myDriver.findElement(By.xpath("//*[@id=\"signCoursesButton\"]")).isDisplayed());
        myDriver.close();
    }

    @Test
    public void verifyThatQaCourseIsPresent() {
        System.setProperty("webdriver.chrome.driver", "/Users/ruslanrakhmonov/IdeaProjects/comseleniumpractice/src/main/resources/chromedriver");
        WebDriver myDriver = new ChromeDriver();
        myDriver.get("https://ithillel.ua/ua");
        myDriver.findElement(By.xpath("//*[@id=\"course\"]")).click();
        assertTrue(myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[1]/ul/li[2]/a")).isDisplayed());
        myDriver.close();
    }

    @Test
    public void verifyThatQaAutomationIsPresent() {
        System.setProperty("webdriver.chrome.driver", "/Users/ruslanrakhmonov/IdeaProjects/comseleniumpractice/src/main/resources/chromedriver");
        WebDriver myDriver = new ChromeDriver();
        myDriver.get("https://ithillel.ua/ua");
        myDriver.findElement(By.xpath("//*[@id=\"course\"]")).click();
        assertTrue(myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[1]/ul/li[2]/a")).isDisplayed());
        myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[1]/ul/li[2]/a")).click();
        assertTrue(myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[3]/div[1]/div/div[13]/section[1]/a[2]/div/h3")).isDisplayed());
        myDriver.close();
    }

    @Test
    public void verifyThatQaAutomationIsOpened() {
        System.setProperty("webdriver.chrome.driver", "/Users/ruslanrakhmonov/IdeaProjects/comseleniumpractice/src/main/resources/chromedriver");
        WebDriver myDriver = new ChromeDriver();
        myDriver.get("https://ithillel.ua/ua");
        myDriver.findElement(By.xpath("//*[@id=\"course\"]")).click();
        assertTrue(myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[1]/ul/li[2]/a")).isDisplayed());
        myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[1]/ul/li[2]/a")).click();
        assertTrue(myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[3]/div[1]/div/div[13]/section[1]/a[2]/div/h3")).isDisplayed());
        myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[3]/div[1]/div/div[13]/section[1]/a[2]/div/h3")).click();
        assertTrue(myDriver.findElement(By.xpath("/html/body/section[1]/div/article/div[1]/div/h1")).isDisplayed());
        myDriver.close();
    }


    @Test
    public void selectKharkivCity() {
        System.setProperty("webdriver.chrome.driver", "/Users/ruslanrakhmonov/IdeaProjects/comseleniumpractice/src/main/resources/chromedriver");
        WebDriver myDriver = new ChromeDriver();
        myDriver.get("https://ithillel.ua/ua");
        myDriver.findElement(By.xpath("//*[@id=\"course\"]")).click();
        assertTrue(myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[1]/ul/li[2]/a")).isDisplayed());
        myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[1]/ul/li[2]/a")).click();
        assertTrue(myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[3]/div[1]/div/div[13]/section[1]/a[2]/div/h3")).isDisplayed());
        myDriver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[3]/div[1]/div/div[13]/section[1]/a[2]/div/h3")).click();
        assertTrue(myDriver.findElement(By.xpath("/html/body/section[1]/div/article/div[1]/div/h1")).isDisplayed());
        myDriver.findElement(By.xpath("//*[@id=\"headerCities\"]/a[4]")).click();
        myDriver.findElement(By.xpath("//*[@id=\"headerCities\"]/a[4]")).isSelected();
        myDriver.close();
    }

    @Test
    public void goToEVO() {
        System.setProperty("webdriver.chrome.driver", "/Users/ruslanrakhmonov/IdeaProjects/comseleniumpractice/src/main/resources/chromedriver");
        WebDriver myDriver = new ChromeDriver();
        myDriver.get("https://ithillel.ua/ua");
        myDriver.findElement(By.xpath("/html/body/header[1]/div/nav[2]/div/ul/li[7]/a")).click();
        assertTrue(myDriver.findElement(By.xpath("/html/body/section[1]/div/article/div[1]")).isDisplayed());
        myDriver.close();
    }

    @Test
    public void evoFillEmail() {
        System.setProperty("webdriver.chrome.driver", "/Users/ruslanrakhmonov/IdeaProjects/comseleniumpractice/src/main/resources/chromedriver");
        WebDriver myDriver = new ChromeDriver();
        myDriver.get("https://ithillel.ua/ua");
        myDriver.findElement(By.xpath("/html/body/header[1]/div/nav[2]/div/ul/li[7]/a")).click();
        assertTrue(myDriver.findElement(By.xpath("/html/body/section[1]/div/article/div[1]")).isDisplayed());
        myDriver.findElement(By.xpath("//*[@id=\"inputSubscribe\"]")).sendKeys("email@email.com");
        myDriver.close();
    }

}
